<?php

use WHMCS\Database\Capsule;

add_hook('DailyCronJob', 1, 'performSync');
/**
 * @return mixed
 *
 * curl all prices
 */
function curlPrices(object $config) {
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, 'https://login.autodns.com');
    curl_setopt($ch,CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt ($ch, CURLOPT_COOKIEJAR, __DIR__ . '/cookie.txt');
    curl_setopt ($ch, CURLOPT_COOKIEFILE, __DIR__ . '/cookie.txt');
    curl_setopt($ch,CURLOPT_POSTFIELDS, 'user=' . urlencode($config->username) . '&password=' . urlencode($config->password));
    curl_exec($ch);
    curl_setopt($ch,CURLOPT_URL, 'https://login.autodns.com/core/proxy.php');
    curl_setopt($ch,CURLOPT_POSTFIELDS, 'start=0&limit=5000&children=0&task=priceregister_list');
    $response = curl_exec($ch);
    return json_decode($response)->data->article_price;
}

/**
 * fetch all Configs
 */
function fetchConfig() : object {
    $list = Capsule::table('tbladdonmodules')->where('module', 'domainpriceSync')->get();
    $result = array();
    foreach($list as $option) {
        $result[$option->setting] = $option->value;
    }
    return (object)$result;
}

/**
 * @param string $path
 * @return bool
 *
 * Start the Code
 */
function performSync() {
    $config = fetchConfig();
    $data = curlPrices($config);
    logModuleCall('domainpriceSync', 'Perform Domain Sync','Sync for ' . count($data) . ' Domains', $data);
    foreach ($data as $domain) {
        try {
            if ($domain->article_type !== 'domain' || preg_match('/[^A-Za-z0-9]/', $domain->article_label)) {
                continue;
            }
            //Collect the prices
            $prices = getPrices($config, $domain);
            if (null === $prices) {
                continue;
            }
            $result = Capsule::table('tbldomainpricing')->where('extension', ".{$domain->article_label}")->first();
            if ($result === null) {
                addDomain($config, $domain, $prices);
            } else {
                updateDomain($result, $prices, $domain);
            }
        } catch (Exception $e) {
            logModuleCall('domainpriceSync', 'Perform Domain Sync', 'Domain ' . $domain->article_label, $e->getMessage());
        }
    }
    logModuleCall('domainpriceSync', 'Perform Domain Sync','Sync for ' . count($data) . ' Domains', 'Done');
}

/**
 * @param mysqli $con
 * @param mysqli_result $result
 * @param array $prices
 * @param object $domain
 *
 * Update the prices of an existing Domain
 */
function updateDomain(object $result, array $prices, object $domain) : void {
    $group = ($domain->price_create_priority === 'DEFAULT' ? 'none' : 'sale');
    Capsule::table('tbldomainpricing')->where('id', $result->id)->update(['group' => $group]);
    Capsule::table('tblpricing')->where('relid', $result->id)->where('type', 'domainregister')->update(['msetupfee' => $prices['create']]);
    Capsule::table('tblpricing')->where('relid', $result->id)->where('type', 'domainrenew')->update(['msetupfee' => $prices['renew']]);
    Capsule::table('tblpricing')->where('relid', $result->id)->where('type', 'domaintransfer')->update(['msetupfee' => $prices['transfer']]);
}

/**
 * @param mysqli $con
 * @param object $domain
 * @param array $prices
 *
 * Add the Domain into the Database
 */
function addDomain(object $config, object $domain, array $prices) : void {
    $group = ($domain->price_create_priority === 'DEFAULT' ? 'none' : 'sale');
    Capsule::table('tbldomainpricing')->insert(
        [
            'extension' => ".{$domain->article_label}",
            'dnsmanagement' => $config->dnsmanagement === 'on',
            'emailforwarding' => $config->emailforwarding === 'on',
            'idprotection' => $config->idprotection === 'on',
            'eppcode' => $config->eppcode === 'on',
            'autoreg' => 'InterNetX',
            'group' => $group
        ]
    );
    $entity = Capsule::table('tbldomainpricing')->where('extension', ".{$domain->article_label}")->first();
    $types = array(
        ['domainregister', 'create'],
        ['domainrenew', 'renew'],
        ['domaintransfer', 'transfer'],
    );
    foreach ($types as $type) {
        Capsule::table('tblpricing')->insert(
            [
                'type' => $type[0],
                'currency' => 1,
                'relid' => $entity->id,
                'msetupfee' => $prices[$type[1]],
                'qsetupfee' => -1,
                'ssetupfee' => -1,
                'asetupfee' => -1,
                'bsetupfee' => -1,
                'tsetupfee' => 0,
                'monthly' => -1,
                'quarterly' => -1,
                'semiannually' => -1,
                'annually' => -1,
                'biennially' => -1,
                'triennially' => 0,
            ]
        );
    }
}

/**
 * @param object $domain
 * @return array
 *
 * Collect all prices for Registration, Renew and Transfer of a Domain
 */
function getPrices(object $config, object $domain) {

    if ($domain->price_update_amount !== '0.00EUR' ||
        null === $domain->price_create_amount ||
        null === $domain->price_renew_amount ||
        null === $domain->price_transfer_amount ||
        !is_array($domain->price_create_amount) ||
        !is_array($domain->price_renew_amount) ||
        !is_array($domain->price_transfer_amount)) {
        return null;
    }
    //Registration of a Domain
    $create = parsePrice($domain->price_create_amount[0]);
    $create = calculatePrice($config, $create);
    //Renew of a Domain
    $renew = parsePrice($domain->price_renew_amount[0]);
    $renew = calculatePrice($config, $renew);
    //Transfer of a Domain
    $transfer = parsePrice($domain->price_transfer_amount[0]);
    $transfer = calculatePrice($config, $transfer);

    if (null === $transfer || null === $renew || null === $create) {
        return null;
    }

    return array(
        'create' => $create,
        'renew' => $renew,
        'transfer' => $transfer
    );
}

/**
 * @param string $price
 * @return float
 *
 * Parse the string price (ex. 3.90 EUR/1y) into an actual float to process it
 */
function parsePrice(string $price) {
    if (strpos($price, '1y')) {
        $price = str_replace(',', '.', substr($price, 0, strpos($price, ' ')));

        return floatval($price);
    }

    return null;
}

/**
 * @param float $price
 * @return float
 *
 * Calculates the VK price with the multipliers defined at the top
 */
function calculatePrice(object $config, $price) {
    if (null === $price) {
        return null;
    }
    if ($price <= 10) {
        $price = $price * (($config->margeA / 100) + 1);
    } else if ($price <= 100) {
        $price = $price * (($config->margeB / 100) + 1);
    } else {
        $price = $price * (($config->margeC / 100) + 1);
    }
    if ($config->prettyPrice === 'on') {
        if ((int)$price != round($price, 2)) {
            $price = ((int)$price) + 0.99;
        }
    } else {
        $price = round($price, 2);
    }

    return $price;
}

