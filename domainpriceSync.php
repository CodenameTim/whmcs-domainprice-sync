<?php
/**
 * WHMCS tawk.to Addon Module
 *
 */
if (!defined('WHMCS')) {
    die('This file cannot be accessed directly');
}

function domainpriceSync_config() {
    $configarray = array(
        'name' => 'InternetX Automated Price/Promo Sync',
        'description' => 'Prices and Promotions of domains are automatically syncronised with your WHMCS prices.',
        'version' => '1.0.0',
        'author' => "<a href='https://timjambor.com/'>Tim Jambor</a>",
        'language' => 'english',
        'fields' => array(
            'username' => array (
                'FriendlyName' => 'Username',
                'Type' =>  'text',
                'Size' => '55',
                'Description' => 'The E-Mail / Username to login at https://login.autodns.com/',
                'Default' => '',
            ),
            'password' => array (
                'FriendlyName' => 'Password',
                'Type' =>  'password',
                'Size' => '55',
                'Description' => 'The Password to login at https://login.autodns.com/',
                'Default' => '',
            ),
            'prettyPrice' => array (
                'FriendlyName' => 'Pretty Price',
                'Type' =>  'yesno',
                'Size' => '55',
                'Description' => 'Set the cents of the final price to 99 to have a price like 5.99 instead of 5.73',
                'Default' => '',
            ),
            'margeA' => array (
                'FriendlyName' => 'Margin < 10',
                'Type' =>  'text',
                'Size' => '55',
                'Description' => 'Margin in percentage to be added for all prices under 10. (ex. price + 10%) NUMBERS ONLY!',
                'Default' => '50',
            ),
            'margeB' => array (
                'FriendlyName' => 'Margin < 100',
                'Type' =>  'text',
                'Size' => '55',
                'Description' => 'Margin in percentage to be added for all prices under 100. (ex. price + 10%) NUMBERS ONLY!',
                'Default' => '30',
            ),
            'margeC' => array (
                'FriendlyName' => 'Margin > 100',
                'Type' =>  'text',
                'Size' => '55',
                'Description' => 'Margin in percentage to be added for all prices above 100. (ex. price + 10%) NUMBERS ONLY!',
                'Default' => '',
            ),
            'dnsmanagement' => array (
                'FriendlyName' => 'DNS Management',
                'Type' =>  'yesno',
                'Size' => '55',
                'Description' => 'Should DNS Management be activated when adding new domains?',
                'Default' => '',
            ),
            'emailforwarding' => array (
                'FriendlyName' => 'Email Forwarding',
                'Type' =>  'yesno',
                'Size' => '55',
                'Description' => 'Should E-Mail Forwarding be activated when adding new domains?',
                'Default' => '',
            ),
            'idprotection' => array (
                'FriendlyName' => 'ID Protection',
                'Type' =>  'yesno',
                'Size' => '55',
                'Description' => 'Should ID Protection be activated when adding new domains?',
                'Default' => '',
            ),
            'eppcode' => array (
                'FriendlyName' => 'EPP Code',
                'Type' =>  'yesno',
                'Size' => '55',
                'Description' => 'Should EPP Code be activated when adding new domains?',
                'Default' => '',
            ),
        )
    );
    return $configarray;
}
